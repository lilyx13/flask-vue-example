import { createRouter, createWebHistory } from "vue-router"
import Home from "../views/Home.vue"


const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/ping",
    name: "ping",
    // lazy loads the page
    component: () => import("../views/Ping.vue")
  },
  {
    path: "/greeting",
    name: "greeting",
    // lazy loading
    component: () => import("../views/Greeting.vue")
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router