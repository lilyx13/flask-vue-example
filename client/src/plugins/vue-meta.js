import { createMetaManager } from 'vue-meta'

const vueMeta = createMetaManager({
  // options go here
})

export default vueMeta