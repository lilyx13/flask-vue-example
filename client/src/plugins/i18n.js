import { createI18n } from 'vue-i18n'

const i18n = createI18n({
  // options go here
})

export default i18n