import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index'
// plugins
import i18n from './plugins/i18n'
import vueMeta from './plugins/vue-meta'



const fedoraApp = createApp(App)

fedoraApp.use(i18n).use(vueMeta).use(router)

fedoraApp.mount("#app")