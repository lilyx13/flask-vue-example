# README

## How to run

### Server
- Navigate to `server/` and set up a env with `python -m venv env`
- Then activate your environment with `. env/bin/activate`
- Now you can boot up your python server with `python main.py`

- then run `pip install requirements.txt`

### Client
- Navigate to  `client/` and run `npm i`  to install all the packages
- run `npm run dev` to fire up the live server
- **note** in order to get data from the backend to show up, you must also be running the python server

---

## Configuration Ideas

Random notes and thoughts about setting things up

### Podman Configuration
1. Create a pod at the root folder

### Server - Client Setup Idea
- Server passes routes for main pages of Fedora's websites
- Navigation within each of them is handled by vue router

#### Example:
- fedoraproject.org (flask)
	- fedoraproject.org/workstation (vue-router)
	- fedoraproject.org/server (vue-router)
- flock.fedoraproject.org (flask)
	- flock.fedoraproject.org/faq (vue-router)

