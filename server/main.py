from flask import Flask, jsonify
from flask_cors import CORS


app = Flask(__name__)

app.config.from_object(__name__)

CORS(app, resources={r"/*":{'origins': "*", "allow_headers": "Access-Control-Allow-Origin"}})
app.debug = True

@app.route("/", methods=["GET"])
def index():
  page_info = "This is the index page"
  return page_info

@app.route('/ping', methods=['GET'])
def ping():
  return("Pong!")

# Greeting Test Route
@app.route('/greeting') 
def greeting():
  name = "Glorious User"
  return name



if __name__ == '__main__':
  app.run()